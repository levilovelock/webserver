package main

import (
	"bitbucket.org/levilovelock/webserver/server"
	"flag"
)

var port = flag.Int("port", 8080, "The port for the server to listen on.")

func main() {
	flag.Parse()
	server.Run(*port)
}
