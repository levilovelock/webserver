-- Create user table
INSERT INTO users (email, password) VALUES
	('test1@aol.com', '$2a$10$O4ccsvA3rCWg2Fl3g32iE.nNXeLxJ0S9sgPYp5nYl6V08kzvfIGIW'), -- p: password
	('lovelock.levi@gmail.com', '$2a$10$Co/BPCmxhr4gJH1Hve.Pxe3E0NG9LlYuukR6uUBWSlxJtNmqW.Tda'), -- p: test
	('joe.blog@gmail.com', '$2a$10$Co/BPCmxhr4gJH1Hve.Pxe3E0NG9LlYuukR6uUBWSlxJtNmqW.Tda'), -- p: test
	('matt.damon@gmail.com', '$2a$10$Co/BPCmxhr4gJH1Hve.Pxe3E0NG9LlYuukR6uUBWSlxJtNmqW.Tda'), -- p: test
	('guyfromfastnfurious@hotmail.com', '$2a$10$Co/BPCmxhr4gJH1Hve.Pxe3E0NG9LlYuukR6uUBWSlxJtNmqW.Tda'), -- p: test
	('vishnu-dengda@gmail.com', '$2a$10$Co/BPCmxhr4gJH1Hve.Pxe3E0NG9LlYuukR6uUBWSlxJtNmqW.Tda'), -- p: test
	('abba-fan-198@gmail.com', '$2a$10$Co/BPCmxhr4gJH1Hve.Pxe3E0NG9LlYuukR6uUBWSlxJtNmqW.Tda'), -- p: test
	('pingmewowgold@cheapbuyyou.org', '$2a$10$Co/BPCmxhr4gJH1Hve.Pxe3E0NG9LlYuukR6uUBWSlxJtNmqW.Tda'); -- p: test


INSERT INTO sessions (sid, uid, expiry) VALUES
	('88ef5766-36c4-49b2-79f4-1abaa5b394f9',1,(TIMESTAMP '2016-02-24 22:21:49.921801')),
	('4d5ab3e3-c18c-4db5-42c8-c462383865f9',2,(TIMESTAMP '2016-02-24 22:22:21.727287')),
	('84936098-cb67-4286-7cdc-caf5d7fce1da',2,(TIMESTAMP '2013-02-24 22:22:21.727287'));
