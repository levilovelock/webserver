package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"

	"log"
)

const (
	POSTGRES = "postgres"
	SQLITE   = "sqlite3"
)

// =============================================================================
// Used for testing initial db connection
func ping(db *gorm.DB) {
	log.Println("Pinging db...")
	err := db.DB().Ping()
	if err != nil {
		panic(err.Error())
	} else {
		log.Print("Success!")
	}
}

// =============================================================================
// Returns a sql.DB object that is to be long lived and injectable
func New(dbType string) *gorm.DB {
	var (
		db  gorm.DB
		err error
	)
	if dbType == POSTGRES {
		db, err = gorm.Open("postgres", "user=llovelock sslmode=disable dbname=odgs")
	} else if dbType == SQLITE {
		db, err = gorm.Open("sqlite3", "/tmp/sqlite.db")
	} else {
		log.Panicf(`Unknown db driver: "%s"`, dbType)
	}
	if err != nil {
		log.Fatal("Error creating database object...", err)
	}

	// Run a ping test
	ping(&db)

	// Configure db
	db.LogMode(true)

	return &db
}
