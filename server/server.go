package server

import (
	"bitbucket.org/levilovelock/webserver/database"
	"bitbucket.org/levilovelock/webserver/session"
	"bitbucket.org/levilovelock/webserver/user"
	"fmt"
	"github.com/codegangsta/inject"
	"github.com/codegangsta/martini"
	"github.com/jinzhu/gorm"
	"net/http"
)

var (
	m *martini.Martini
	i inject.Injector
	d *gorm.DB
)

// =============================================================================
// Initialiser for the server!!!
func init() {
	m = martini.New()
	i = m.Injector
	d = database.New(database.SQLITE)
	// d = database.New(database.POSTGRES)

	// Setup middleware (Injectable services)
	m.Use(martini.Recovery())
	m.Use(martini.Logger())
	m.Use(SessionAuth())

	// Setup User Routes
	r := martini.NewRouter()
	r.Any("/", getIndex)
	r.Any("/index.html", getIndex)

	//==========================================================================
	// Database Controllers
	//==========================================================================

	// Map the general database connector, to be injected into db controllers
	i.Map(d)

	// Create User db controller
	userdbc := user.UserDBControllerGorm{}
	i.Apply(&userdbc)
	userdbc.Init()
	i.MapTo(userdbc, new(user.UserDBController))

	// Create Session db controller
	sessiondbc := session.SessionDBControllerGorm{}
	i.Apply(&sessiondbc)
	sessiondbc.Init()
	i.MapTo(sessiondbc, new(session.SessionDBController))

	//==========================================================================
	// Managers
	//==========================================================================

	usermgr := new(user.UserManager)
	i.Apply(usermgr)
	i.Map(usermgr)

	sessionmgr := new(session.SessionManager)
	i.Apply(sessionmgr)
	i.Map(sessionmgr)

	// Add the router action
	m.Action(r.Handle)
}

// =============================================================================
// External entry point for the server
func Run(port int) {
	// Ensure db gets closed
	defer d.DB().Close()
	// Start web server
	fmt.Println("listening on port", port)
	address := fmt.Sprintf("localhost:%d", port)
	http.ListenAndServe(address, m)
}

// =============================================================================
// Index response
func getIndex() string {
	fmt.Println("listening on port 8080")

	response := "This webserver is used only in a RESTful way, please adhere to the API."
	return response
}
