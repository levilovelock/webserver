package server

import (
	"bitbucket.org/levilovelock/webserver/session"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"testing"
	"time"
)

var (
	baseUrl   string = "http://localhost:8090"
	setupDone bool   = false
)

func setup() {
	if !setupDone {
		// Start the webserver
		go Run(8090)
		// Allow it to start
		time.Sleep(10 * time.Millisecond)

		setupDone = true
	}
}

func login(username, password string) (*http.Response, error) {
	setup()

	// Use make here because url.Values is literally just a map[string][]string anyway
	postParams := make(url.Values)
	postParams.Set("u", username)
	postParams.Set("p", password)

	// Make request
	return http.PostForm(baseUrl, postParams)
}

func loginWithCookiesAndReturnStatusCode(sid string, uid int) int {
	setup()

	sidCookie := &http.Cookie{Name: session.SESSION_COOKIE_KEY, Value: sid}
	uidCookie := &http.Cookie{Name: session.USER_COOKIE_KEY, Value: strconv.Itoa(uid)}

	parsedUrl, _ := url.Parse(baseUrl)

	client := new(http.Client)

	r, _ := http.NewRequest("GET", parsedUrl.String(), nil)

	// r.Header.Set("Content-Type", "application/json;")
	r.AddCookie(sidCookie)
	r.AddCookie(uidCookie)

	// Make request and defer the closing
	resp, _ := client.Do(r)
	defer resp.Body.Close()

	return resp.StatusCode
}

func TestNoAuthFailure(t *testing.T) {
	setup()

	resp, _ := http.Get(baseUrl)

	// Close the response body after done
	defer resp.Body.Close()

	// Read the entire body
	body, _ := ioutil.ReadAll(resp.Body)

	// Convert body to a string
	body_s := string(body)

	// We expect a 401 as we're not authorising at all
	if !strings.Contains(body_s, "Not Authorized") {
		t.Errorf("Body:", string(body))
	}
}

func TestBadUserAuthFailure(t *testing.T) {
	resp, _ := login("badusername", "password")

	// Close the response body after done
	defer resp.Body.Close()

	// We expect a 401 response
	if resp.StatusCode != 401 {
		t.Errorf("Expected 401 return code, recieved %d", resp.StatusCode)
	}
}

func TestBadUserAndPassCombinationAuthFailure(t *testing.T) {
	resp, _ := login("lovelock.levi@gmail.com", "badpass")

	// Close the response body after done
	defer resp.Body.Close()

	// We expect a 401 response
	if resp.StatusCode != 401 {
		t.Errorf("Expected 401 return code, recieved %d", resp.StatusCode)
	}
}

func TestFreshAuthUsernamePasswordSuccess(t *testing.T) {
	resp, _ := login("lovelock.levi@gmail.com", "test")

	// Close the response body after done
	defer resp.Body.Close()

	// We expect a 200 response
	if resp.StatusCode != 200 {
		t.Errorf("Expected 200 return code, recieved %d", resp.StatusCode)
	}

	// Check for a new cookie
	cookies := resp.Cookies()

	if len(cookies) != 2 {
		t.Errorf("Expected 2 cookies to be returned, recieved %d", len(cookies))
	}
}

func TestBadSessionIdFailure(t *testing.T) {
	// We expect a 401 response because the session id could not be found in the db
	if resp := loginWithCookiesAndReturnStatusCode(`really-bad-session-id`, 0); resp != 401 {
		t.Errorf(`Expected "%d" status code, instead received "%d"`, 401, resp)
	}
}

func TestBadSessionIdAndUserIdFailure(t *testing.T) {
	// We expect a 401 response because the session does not match the user id in db
	if resp := loginWithCookiesAndReturnStatusCode(`88ef5766-36c4-49b2-79f4-1abaa5b394f9`, 100); resp != 401 {
		t.Errorf(`Expected "%d" status code, instead received "%d"`, 401, resp)
	}
}

func TestBadSessionExpiryFailure(t *testing.T) {
	// We expect a 401 response because the session in db has expired
	if resp := loginWithCookiesAndReturnStatusCode(`84936098-cb67-4286-7cdc-caf5d7fce1da`, 2); resp != 401 {
		t.Errorf(`Expected "%d" status code, instead received "%d"`, 401, resp)
	}
}

func TestSessionAuthSuccess(t *testing.T) {
	// We expect a 200 response because the session and uid in db match the cookies
	if resp := loginWithCookiesAndReturnStatusCode(`4d5ab3e3-c18c-4db5-42c8-c462383865f9`, 2); resp != 200 {
		t.Errorf(`Expected "%d" status code, instead received "%d"`, 200, resp)
	}
}
