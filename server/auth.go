package server

import (
	"bitbucket.org/levilovelock/webserver/session"
	"bitbucket.org/levilovelock/webserver/user"
	"github.com/codegangsta/martini"
	"log"
	"net/http"
	"strconv"
)

// SessionAuth returns a Handler that authenticates via Sessions
func SessionAuth() martini.Handler {

	return func(res http.ResponseWriter, req *http.Request, l *log.Logger, umgr *user.UserManager, smgr *session.SessionManager) {

		// Check for cookie, if has then validate
		seshCookie, errSesh := req.Cookie(session.SESSION_COOKIE_KEY)
		if errSesh == nil {
			userCookie, errUser := req.Cookie(session.USER_COOKIE_KEY)
			if errUser == nil {
				userCookieVal, userCookieValErr := strconv.ParseInt(userCookie.Value, 10, 64)
				if userCookieValErr == nil {
					user, userRetrievalError := umgr.GetUserById(userCookieVal)
					if userRetrievalError == nil && smgr.CheckValidSession(seshCookie.Value, *user) {
						return
					}
				}
			}
		}

		// Else check for POST uname/pword
		if req.Method == "POST" {
			uname := req.PostFormValue("u")
			pass := req.PostFormValue("p")

			if uname != "" && pass != "" {
				u, err := umgr.GetUserAndAuthoritate(uname, pass)
				// If match up then create new session and return
				if err == nil {
					l.Printf(`Successfully authenticated user "%s" (id: %d) - Creating new Session.`, u.Email, u.Id)
					s, err := smgr.CreateSession(*u)

					// Write the cookies if session was created successfully
					if err == nil {
						http.SetCookie(res, &http.Cookie{Name: "user", Value: strconv.FormatInt(s.UserId, 10), Expires: s.Expires})
						http.SetCookie(res, &http.Cookie{Name: "sesh", Value: s.Id, Expires: s.Expires})
						return
					}
				} else {
					// TODO: Clean up all the error (logging)
					l.Println(err)
				}
			}
		}

		// Fail otherwise.
		// res.Header().Set("WWW-Authenticate", "Basic realm=\"Authorization Required\"")
		http.Error(res, "Not Authorized", http.StatusUnauthorized)

	}
}
