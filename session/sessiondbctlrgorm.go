package session

import (
	"github.com/jinzhu/gorm"
	"log"
)

type SessionDBControllerGorm struct {
	// "inject" tag is for DI, the field needs to be externally accessible for injection too.
	DB *gorm.DB    `inject`
	L  *log.Logger `inject`
}

// =============================================================================
//
// Session Database Controller Service Functions
//
// =============================================================================

func (s SessionDBControllerGorm) Init() {
	// Do init stuff
	s.DB.CreateTable(Session{})
}

func (s SessionDBControllerGorm) Create(session Session) error {

	stringStmt := `INSERT INTO sessions (sid, uid, expiry) VALUES($1, $2, $3)`
	sqlStmt, err := s.DB.DB().Prepare(stringStmt)
	defer sqlStmt.Close()
	if err != nil {
		return err
	}

	res, err := sqlStmt.Exec(session.Id, session.UserId, session.Expires)
	if err != nil {
		s.L.Println(`Failed to exec sql for query "%s", with params "%s", "%d", "%s"`, stringStmt, session.Id, session.UserId, session.Expires.String())
		return err
	}

	rows, _ := res.RowsAffected()
	s.L.Println("SQL complete! Affected this many rows: ", rows)

	return nil
}

func (s SessionDBControllerGorm) GetSession(id string) (*Session, error) {
	session := &Session{}
	err := s.DB.Where("Id = ?", id).Find(session).Error

	if err == nil {
		return nil, err
	}

	return session, nil
}
