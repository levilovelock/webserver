package session

import (
	"bitbucket.org/levilovelock/webserver/user"
	"github.com/nu7hatch/gouuid"
	"log"
	"time"
)

type SessionManager struct {
	DB SessionDBController `inject`
	L  *log.Logger         `inject`
}

func (s *SessionManager) CheckValidSession(sid string, user user.User) bool {
	session, err := s.DB.GetSession(sid)
	if err != nil {
		s.L.Println(err)
		return false
	}
	return session.IsValid() && session.UserId == user.Id
}

func (s *SessionManager) CreateSession(user user.User) (*Session, error) {
	sessionExpiry := time.Now().Add(SESSION_LENGTH)

	sid, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	session := &Session{Id: sid.String(), UserId: user.Id, Expires: sessionExpiry}

	err = s.DB.Create(*session)
	if err != nil {
		return nil, err
	} else {
		return session, nil
	}
}
