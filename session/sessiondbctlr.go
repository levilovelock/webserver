package session

type SessionDBController interface {
	Init()
	Create(session Session) error
	GetSession(id string) (*Session, error)
}
