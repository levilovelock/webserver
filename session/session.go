package session

import (
	"time"
)

const (
	SESSION_COOKIE_KEY string        = `sesh`
	USER_COOKIE_KEY    string        = `user`
	SESSION_LENGTH     time.Duration = time.Hour * 24 * 7 * 2
)

type Session struct {
	Id      string `sql:"type:varchar(36);primary key"`
	UserId  int64
	Expires time.Time
}

// Utility functions
func (s *Session) IsValid() bool {
	return s.Expires.After(time.Now())
}
