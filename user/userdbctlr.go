package user

type UserDBController interface {
	Init()
	GetUserLogin(email string) (*User, error)
	GetUserById(id int64) (*User, error)
	InsertUser(email, password string) (*User, error)
}
