package user

import (
	"code.google.com/p/go.crypto/bcrypt"
	"errors"
	"fmt"
	"log"
	"regexp"
)

type UserManager struct {
	DB UserDBController `inject`
	L  *log.Logger      `inject`
}

func (u *UserManager) GetUserById(id int64) (*User, error) {
	user, err := u.DB.GetUserById(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u *UserManager) GetUserAndAuthoritate(email, password string) (*User, error) {
	user, err := u.DB.GetUserLogin(email)
	if err != nil {
		return nil, err
	}

	if bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)) != nil {
		return nil, errors.New("Username password auth failed")
	}

	user.Password = ""

	return user, nil
}

func (u *UserManager) CreateUser(email, password string) (*User, error) {

	if err := validateUserEmail(email); err != nil {
		return nil, err
	}

	if err := validateUserPassword(password); err != nil {
		return nil, err
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	user, err := u.DB.InsertUser(email, string(hashedPassword))
	if err != nil {
		return nil, err
	} else {
		return user, nil
	}
}

// =============================================================================
// User Utility Functions
// =============================================================================

func validateUserEmail(email string) error {

	validEmail := regexp.MustCompile(`^[a-zA-Z0-9\-\_\.]+@[a-zA-Z0-9\-\_]+\.[a-zA-Z0-9\-\_\.]+$`)

	if !validEmail.MatchString(email) {
		return errors.New("Invalid email address!")
	}

	return nil
}

func validateUserPassword(password string) error {
	if len(password) < MIN_PASSWORD_LEN || len(password) > MAX_PASSWORD_LEN {
		return errors.New(fmt.Sprintf(`Password too short, needs to be at least "%d" characters long.`, MIN_PASSWORD_LEN))
	}
	return nil
}
