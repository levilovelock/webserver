package user

import (
	"errors"
	"github.com/levilovelock/testify/assert"
	"log"
	"os"
	"testing"
)

var (
	setupDone bool = false
	logger    *log.Logger
	umgr      *UserManager
)

func setup(t *testing.T) {
	if !setupDone {
		// Manually set up objects with a mock dbc
		logger = log.New(os.Stdout, "[test] ", 0)
		umgr = &UserManager{new(DefaultDBCMock), logger}
	}
}

// =============================================================================
// Mocks
type DefaultDBCMock struct{ t *testing.T }

func (e DefaultDBCMock) Init()                                            {}
func (e DefaultDBCMock) GetUserLogin(email string) (*User, error)         { return nil, nil }
func (e DefaultDBCMock) GetUserById(id int64) (*User, error)              { return nil, nil }
func (e DefaultDBCMock) InsertUser(email, password string) (*User, error) { return nil, nil }

type MockInsertUserNotCalled struct {
	DefaultDBCMock
}

func (m *MockInsertUserNotCalled) InsertUser(email, password string) (*User, error) {
	m.t.Fatal("Method insertUser has been called")
	return new(User), nil
}

type MockInsertUserCalled struct {
	DefaultDBCMock
	counter int
}

func (m *MockInsertUserCalled) InsertUser(email, password string) (*User, error) {
	m.counter++
	return new(User), nil
}

type MockInsertUserFail struct {
	DefaultDBCMock
}

func (m *MockInsertUserFail) InsertUser(email, password string) (*User, error) {
	return nil, errors.New("Failed DB query.")
}

type MockGetUserById struct {
	DefaultDBCMock
}

func (m *MockGetUserById) GetUserById(id int64) (*User, error) {
	if id == 1 {
		return &User{Id: 1, Email: "expectedemail"}, nil
	}
	return nil, errors.New("asdf")
}

type MockGetUserLogin struct {
	DefaultDBCMock
}

func (m *MockGetUserLogin) GetUserLogin(email string) (*User, error) {

	if email == "expectedemail" {
		// Create a user with bcrypted password of 'expectedpassword'
		u := &User{Id: 1, Email: email, Password: "$2a$10$JyKxIaf4QeQQF3ZHxQEZW.3S2nHRwsrsRVjK8WMJlurd5/duY0WQK"}
		return u, nil
	}

	return nil, errors.New("db failure")
}

// =============================================================================

func TestGetUserAndAuthoritateHasEmptyPassword(t *testing.T) {
	setup(t)
	mockDB := MockGetUserLogin{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	user, _ := umgr.GetUserAndAuthoritate("expectedemail", "expectedpassword")
	assert.Empty(t, user.Password)
}

func TestGetUserAndAuthoritateAuthSuccess(t *testing.T) {
	setup(t)
	mockDB := MockGetUserLogin{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	email := "expectedemail"
	user, err := umgr.GetUserAndAuthoritate(email, "expectedpassword")

	assert.Nil(t, err)
	assert.Equal(t, int64(1), user.Id)
	assert.Equal(t, email, user.Email)
}

func TestGetUserAndAuthoritateAuthFail(t *testing.T) {
	setup(t)
	mockDB := MockGetUserLogin{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	_, err := umgr.GetUserAndAuthoritate("expectedemail", "badpassword")

	assert.NotNil(t, err)
	assert.Equal(t, "Username password auth failed", err.Error())
}

func TestGetUserAndAuthoritateDbFail(t *testing.T) {
	setup(t)
	mockDB := MockGetUserLogin{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	_, err := umgr.GetUserAndAuthoritate("bademail", "password")

	assert.NotNil(t, err)
	assert.Equal(t, "db failure", err.Error())
}

func TestGetUserByIdSuccess(t *testing.T) {
	setup(t)
	mockDB := MockGetUserById{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	var validId int64 = 1
	user, err := umgr.GetUserById(validId)

	assert.Nil(t, err)
	assert.Equal(t, int64(1), user.Id)
	assert.Equal(t, "expectedemail", user.Email)
}

func TestGetUserByIdFail(t *testing.T) {
	setup(t)
	mockDB := MockGetUserById{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	var invalidId int64 = 2
	_, err := umgr.GetUserById(invalidId)
	assert.NotNil(t, err)
}

func TestCreateNewUserInvalidPassword(t *testing.T) {
	setup(t)
	mockDB := MockInsertUserNotCalled{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	invalidPasswords := []string{
		"passwordthatiswaytoolongbecauseithastoomanycharactersanditisjustridiculous",
		"passwor",
		"p",
		"",
	}
	for _, pass := range invalidPasswords {
		_, err := umgr.CreateUser("user@email.com", pass)
		assert.NotNil(t, err)
	}
}

func TestCreateNewUserInvalidEmail(t *testing.T) {
	setup(t)
	mockDB := MockInsertUserNotCalled{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	invalidEmails := []string{
		"a",
		"asdf@",
		"@",
		"asdf@asdf",
		"a@.",
		"@.",
		"@.c",
		".c",
		"2@cd.",
		"2@.cd",
		"@dc.c",
		"",
	}
	for _, email := range invalidEmails {
		_, err := umgr.CreateUser(email, "validpassword")
		assert.NotNil(t, err)
	}
}

func TestCreateValidEmailAddresses(t *testing.T) {
	setup(t)
	mockDB := MockInsertUserCalled{DefaultDBCMock{t}, 0}
	umgr.DB = &mockDB

	validEmails := []string{
		"test.test@gmail.com",
		"test.test@g.mail.ac.uk",
		"asdf@gmail.com",
		"19ik.d__-@hfdih8923nr.z",
	}
	for _, email := range validEmails {
		_, err := umgr.CreateUser(email, "validpassword")
		assert.Nil(t, err)
	}

	assert.Equal(t, len(validEmails), mockDB.counter)
}

func TestCreateValidPasswords(t *testing.T) {
	setup(t)
	mockDB := MockInsertUserCalled{DefaultDBCMock{t}, 0}
	umgr.DB = &mockDB

	validPasswords := []string{
		"12345678",
		"password09",
	}
	for _, password := range validPasswords {
		_, err := umgr.CreateUser("email@address.com", password)
		assert.Nil(t, err)
	}

	assert.Equal(t, len(validPasswords), mockDB.counter)
}

func TestInsertFail(t *testing.T) {
	setup(t)
	mockDB := MockInsertUserFail{DefaultDBCMock{t}}
	umgr.DB = &mockDB

	_, err := umgr.CreateUser("valid@email.address", "validpassword")
	assert.NotNil(t, err)
	assert.Equal(t, "Failed DB query.", err.Error())
}
