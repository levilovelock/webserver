package user

import (
	"github.com/jinzhu/gorm"
	"log"
)

type UserDBControllerGorm struct {
	// "inject" tag is for DI, the field needs to be externally accessible for injection too.
	DB *gorm.DB    `inject`
	L  *log.Logger `inject`
}

func (u UserDBControllerGorm) Init() {
	// Do init stuff
	u.DB.CreateTable(User{})
}

// =============================================================================
//
// User Database Controller Service Functions
//
// =============================================================================

func (u UserDBControllerGorm) GetUserLogin(email string) (*User, error) {
	u.L.Println("Searching for user with email:", email)

	var id int64
	var password string

	err := u.DB.DB().QueryRow(`SELECT id, password FROM users WHERE email = $1`, email).Scan(&id, &password)
	if err != nil {
		return nil, err
	}

	return &User{Id: id, Email: email, Password: password}, nil
}

func (u UserDBControllerGorm) GetUserById(id int64) (*User, error) {
	user := new(User)

	err := u.DB.DB().QueryRow("SELECT id, email, password FROM users WHERE id = $1", id).Scan(&user.Id, &user.Email, &user.Password)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u UserDBControllerGorm) InsertUser(email, password string) (*User, error) {

	user := &User{Email: email, Password: password}

	err := u.DB.DB().QueryRow(`INSERT INTO users (email, password) VALUES($1, $2) returning id`, email, password).Scan(&user.Id)
	if err != nil {
		return nil, err
	}

	return user, nil
}
