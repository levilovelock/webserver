package user

type User struct {
	Id       int64  `json:"id"`
	Email    string `json:"email"`
	Password string
}

const (
	MIN_PASSWORD_LEN = 8
	MAX_PASSWORD_LEN = 60
)
